# AcubeSAT Frontend Workshop

This repository contains the source code of the OPS-GPT application used for the Frontend Workshop. The app consists of two parts: the Angular Frontend application and the backend application (app.py).

The frontend part of the project (all the files except the app.py file) contains the source code that produces the user interface of the app and handles communication with the backend application.

The backend (app.py) performs the backend logic of the app using the Flask framework. The app also communicates with OpenAI's model APIs using the openai library.

## Dependencies 

To run the OPS-GPT, you will need to install several dependencies:
 ### nodejs
 Node.js is a JavaScript runtime environment that executes JavaScript code outside of a web browser. To install Node.js on Ubuntu, you can use the Node Version Manager (NVM), a bash script used to manage multiple versions of Node.js. First, install NVM using the following steps:

1. Open the console and use the following command:

    ```bash
        sudo apt-get install wget
    ```

2. To install a different version, replace v0.39.0 with another value or use the following command to download the latest release:

    ```bash
    wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.39.0/install.sh | bash
    ```

3. Allow the NVM script to run from your user’s bash profile:

    ```bash
    source ~/.profile
    ```


After NVM is installed, you can start installing Node.js on your system:

1. Check all the available versions of Node.js using NVM by running the following command:

    ```bash
    nvm ls-remote
    ```


2. While you may install any Node.js release, we recommend using the latest version to ensure its support and reliability. For this example, we will install the latest LTS version at the time of writing: 

    ```bash
    nvm install 16.17.0
    ```


3. Check whether it has been successfully installed by querying the currently active version number:

    ```bash
    node -v
    ```

### Angular-CLI

Angular CLI is a command-line interface tool for developing Angular applications. To install Angular CLI, run the following command in your terminal:

```bash
sudo npm install -g @angular/cli
```

### Git 

Git is a version control system used for tracking changes in source code. If Git is not already installed on your computer, you can install it using the following command:

```bash
sudo apt install git
```


### Pip

Pip is a tool for installing Python packages. To install pip run the following command:
```bash
sudo apt install python3-pip
```

### Flask

Flask is a web framework for Python. To install Flask, run the following command:

```bash
pip install flask
```

### OpenAI

The OpenAI Python library provides convenient access to the OpenAI API from Python applications. To install it, run the following command:

```bash
pip install --upgrade openai
```

In order to be able to access the OpenAI API from within your Python script we need to use an API key. To retrieve your OpenAI API key you need to create a user account at https://openai.com/ and access the API Keys section in the OpenAI dashboard to create a new API key.

## How to clone this repo

In order to clone this repo in your computer, you just need to run the following command(assuming Git already installed):

```bash
git clone https://gitlab.com/acubesat/education/frontend-workshop.git
``` 
After cloning the repository and opening it in your editor, install the Angular app dependencies using the following command:

```bash
npm install
```
## How to run the app

To run the frontend application run the following command:

```bash
ng serve
```

Then run the backend app using the following command:

```bash
python3 app.py
```

