import string
from flask import Flask
from flask import request
from flask import jsonify
import datetime
import os
import openai
import json
from flask_cors import CORS


x = datetime.datetime.now()
i = 0
content = ""
history = []
# Initializing flask app
app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})


@app.route('/post', methods=['POST'])
def add_question():
    question = request.get_json()

    print("Question is" + question['body'])

    f = open("demofile2.txt", "a")
    f.write(question['body'])
    f.close()
    return 'Done', 201


@app.route('/add', methods=['POST'])
def add_history():
    history.append(request.get_json())

    f = open("demofile1.txt", "a")
    f.write(str(history))
    f.close

    return 'Done', 201

# Route for seeing a data


@app.route('/data')
def get_data():

    f = open("demofile2.txt", "r")
    prompt = f.read(20)
    f.close()
    f = open("demofile2.txt", "w")
    f.close()
    print("Prompt is" + prompt)
    openai.api_key = "YOUR_API_KEY"
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[
            {"role": "system", "content": "You are a helpful assistant."},
            {"role": "user", "content": prompt}
        ]
    )
    print(response.choices[0].message.content)

    return response.choices[0].message.content


if __name__ == '__main__':
    app.run(debug=True)
