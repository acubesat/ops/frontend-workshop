import { Component } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { Messages } from 'src/app/Messages';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
})
export class ChatComponent {
  
  messages: Messages[] = [
    //Add a welcome message
  ]; //Array that stores the questions and the answers

  data!: string; //stores the answer from the get request

  question: string = ''; //this variable is bound to the input field

  constructor( private chatService: ChatService ) {}

    /**
     * Implement the onSubmit function that: 
     * 1. Call the chatService which makes a post request to the backend.
     * 2. Adds the questions to the messages array.
     * 3. Fetches the answer from the backend inside the async function. You will need to use 'await lastValueFrom()' in order to resolve the promise.
     * 4. Adds the OPS-GPT's answer to the messages array.
     */

  onSubmit(): void {
    setTimeout(async () => {

    }, 1000);
  }

  /**
   * This function is primarily used in the .css file and affects the design
   * @param message the message which we want to get the class from
   * @returns the message's class
   */

  getClass(message: Messages): string {
    if (message.name == 'OPS-GPT') {
      return 'bot';
    } else {
      return 'human';
    }
  }

  /**
   * This function is primarily used in the .css file and affects the design
   * @param message the message which we want to get the color from
   * @returns the message's color
   */

  getColor(message: Messages): string {
    if (message.name == 'OPS-GPT') {
      return 'color-bot';
    } else {
      return 'color-human';
    }
  }
}
