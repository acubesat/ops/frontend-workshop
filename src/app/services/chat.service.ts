import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ChatService {

  dataUrl: string = 'http://127.0.0.1:5000/data'; //the url for the get request
  postUrl: string = 'http://127.0.0.1:5000/post'; //the url for the post request
 
  constructor(private http: HttpClient) {}

  /**
   * Implement the getAnswer function:
   * Makes a get request, using the HttpClient, to the backend to retrieve the answer.
   * The response type of the get request needs to be 'text'.
   * @returns A string observable of the answer 
   */

  getAnswer() {

  }

  /**
   * Makes a post request to the backend sending the question. You will need to subscribe to this request. 
   * The response type of the request needs to be 'text'.
   * @param question the question we want to send to the backend.
   */

  postQuestion(question: string): void {

  }
}
